# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
SUPPORT_PYTHON_ABIS="1"

inherit distutils

DESCRIPTION="Python threaded library to handle HAL devices and their events"
HOMEPAGE="http://pypi.python.org/pypi/minihallib/"
SRC_URI="http://pypi.python.org/packages/source/m/${PN}/${PF}.tar.gz"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE="examples"

DOCS="README"

DEPEND=""
RDEPEND="dev-python/dbus-python"
RESTRICT_PYTHON_ABIS="3.*"

src_install() {
	distutils_src_install

	if use examples; then
		insinto /usr/share/doc/"${PF}"/
		doins -r "examples"
	fi
}
