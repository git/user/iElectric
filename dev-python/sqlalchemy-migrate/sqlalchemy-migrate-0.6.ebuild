# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/sqlalchemy-migrate/sqlalchemy-migrate-0.5.4.ebuild,v 1.4 2010/07/09 12:28:58 arfrever Exp $

EAPI="3"
PYTHON_DEPEND="2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit distutils eutils

DESCRIPTION="SQLAlchemy Schema Migration Tools"
HOMEPAGE="http://code.google.com/p/sqlalchemy-migrate/ http://pypi.python.org/pypi/sqlalchemy-migrate"
SRC_URI="http://${PN}.googlecode.com/files/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/decorator
	dev-python/tempita
	dev-python/setuptools
	dev-python/sqlalchemy"
RDEPEND="${DEPEND}"

PYTHON_MODNAME="migrate"


pkg_postinst() {
	distutils_pkg_postinst

	einfo
	einfo "Be sure to read changelog for backward incompatible changes"
	einfo "http://packages.python.org/sqlalchemy-migrate/changelog.html#backwards-06"
	einfo
}

