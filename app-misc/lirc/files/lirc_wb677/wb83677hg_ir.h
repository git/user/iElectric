#define IR_IOC_MAGIC           'i'

/* output chip registers for debug */
#define IR_DUMPCIRREG    _IO(IR_IOC_MAGIC, 0x80)
#define IR_DUMPWAKEREG   _IO(IR_IOC_MAGIC, 0x81)


/* study wake key on MCE CIR controller */
#define IR_IOLEARNWAKE   _IO(IR_IOC_MAGIC, 0x82)

/* disable cir wake */
#define IR_IOUNSETWAKE   _IO(IR_IOC_MAGIC, 0x83)

/* enable cir wake */
#define IR_IOSETWAKE     _IO(IR_IOC_MAGIC, 0x84)


/* get carrier for study ir signal */
#define IR_IOGETCARRIER  _IOR(IR_IOC_MAGIC, 0x85, unsigned int)

/* set carrier for send ir signal */
#define IR_IOSETCARRIER  _IOW(IR_IOC_MAGIC, 0x86, unsigned int)

/* start/stop study key */
#define IR_IOSTARTSTUDY  _IO(IR_IOC_MAGIC, 0x88)
#define IR_IOSTOPSTUDY   _IO(IR_IOC_MAGIC, 0x89)

/* study key buf len */
#define IR_IOSTUDYLEN    _IOR(IR_IOC_MAGIC, 0x8A, unsigned int)

/* study key buf data */
#define IR_IOSTUDYBUF    _IOR(IR_IOC_MAGIC, 0x8B, unsigned char)

/* restudy key buf data */
#define IR_IORESTUDYBUF  _IO(IR_IOC_MAGIC, 0x8C)


/* clean data */
#define IR_IOCLEANDATA   _IO(IR_IOC_MAGIC, 0x8E)


/* check chip identification for the applications which are related to hardware */
#define IR_CHECKCHIP     _IOR(IR_IOC_MAGIC, 0x8F, unsigned long long)

