#ifndef __LIRC_WB677_COMMON_EXTERN_HEADER__
#define __LIRC_WB677_COMMON_EXTERN_HEADER__

#include <linux/semaphore.h>

#include "/usr/src/lirc-0.8.6/drivers/lirc.h"
#include "/usr/src/lirc-0.8.6/drivers/kcompat.h"
#include "/usr/src/lirc-0.8.6/drivers/lirc_dev/lirc_dev.h"

extern char *DRVNAME;

/* chip id string, at most 7 characters */
extern char *chip_id;




/* allow show debug messages
 * without additional debug setting, only show current IRQ number, I/O port and final decode output */
/*//#define IR_667_DEBUG*/

/* show debug message while decode mce keyboard and mouse signals */
#ifdef IR_667_DEBUG
/*//#define ALLOW_DEBUG_DECODE*/
#endif

/* show incoming interrupt signals
 * may block machine, mask it if sure interrupt signal is good */
#ifdef IR_667_DEBUG
/*//#define ALLOW_DEBUG_INTERRUPT*/
#endif

/* show received infrared pulse and cancel decode functions, mask it in stable version
 * it will stop decode MCE controller, keyboard and mouse
 * mask it if want to decode incoming signals
 */
#ifdef IR_667_DEBUG
/*//#define ALLOW_DEBUG_PRINT_PULSE*/
#endif

/* show debug message while set WAKE UP key
 * it will print cir_wake buf message while setting wake up key
 * mask it in stable version
 */
#ifdef IR_667_DEBUG
#define ALLOW_DEBUG_WAKE
#endif

/* show STUDY debug messages */
#ifdef IR_667_DEBUG
#define ALLOW_DEBUG_STUDY
#endif

/* show ioctl debug messages */
#ifdef IR_667_DEBUG
#define ALLOW_DEBUG_IOCTL
#endif

/* step debug */
#ifdef IR_667_DEBUG
#define ALLOW_DEBUG_STEP
#endif

#ifdef ALLOW_DEBUG_STEP
#define STEP_DBG \
	do {		\
		printk("w83667hg_ir step debug: %s, %d\n", __FILE__, __LINE__); \
	} while (0);
#else
#define STEP_DBG
#endif

#ifdef ALLOW_DEBUG_PRINT_PULSE
/* print current received ir pulse
 * it will output all data, clean CIR_IRSTS so make all following irq_handler ignore
 */
void debug_print_pulse(void);

void debug_print_wake_pulse(void);
#endif




/* register lirc device & buffer */

/* mininum value 76, recommend 256 */
#define IRCTL_BUF_LIMIT 2048


struct irctl {
	struct input_dev *input_dev; /* allocate by input_init */
	struct lirc_buffer *lirc_buffer;
	struct lirc_driver *lirc_plugin;
	spinlock_t lock;
	/* for controller */
	lirc_t lircdata;
	u8 ctrl_fix_head; /* fix controller not sync problem*/
	/* for keyboard */
	u8 pressed_keycode;	/* initialize as 0*/
	u8 pressed_shiftmask;	/* initialize as 0*/
	/* for decode */
	u8 buf[IRCTL_BUF_LIMIT];
	unsigned int buf_count; /* init as 0, reset 0 after decode*/
	unsigned int cur_buf_num; /* init as 0, set as 0 while clean*/
	/* for study */
#define ST_STUDY_NONE      0x0
#define ST_STUDY_START     0x01
#define ST_STUDY_CARRIER   0x02
#define ST_STUDY_ALL_RECV  0x04
	u8 study_state;
	/* carrier period = 1 / frequency */
	unsigned int carrier;
	wait_queue_head_t queue;
};

extern unsigned int usb_kbd_keycode[256];

#endif
