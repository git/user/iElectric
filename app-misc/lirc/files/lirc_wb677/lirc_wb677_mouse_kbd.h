#ifndef __LIRC_WB677_MOUSE_KBD_HEADER__
#define __LIRC_WB677_MOUSE_KBD_HEADER__




/* jump asked frame number, can use on key, mouse and controller signals
 *
 * after jump frame_num, current frame number = frame_num + 1
 * record current buf number of irctl buf
 * record current pulse's bit and decode_status
 *
 * problem 1:
 * When jump_frame() stops on a two_width_len pulse, the buf_num will be the
 * second pulse of the double_width_len pulse.
 * So jump_frame() must increase buf_num 1 on problem 1.
 * For sample period is 50us, a 444us piulse's minimal sample value is 7, and
 * two piulses' minimal sample value is 15. So just increase buf_num 1 works
 * fine. If sample period changes, this solution also need change.
 *
 * one_width_len pulse:
 *   _   _
 * .| |_| |_.      (1010)
 *     ^
 *     |
 *   stop: jumped frame_num
 *
 *
 * problem 1: double_width_len pulse:
 *   __    _
 * .|  |__| |.      (11001)
 *       ^
 *       |
 *      stop: jumped frame_num + 1, buf_num + 1
 *
 * so if cur buf len > TWO_PULSE_LEN_LOW, buf_num --
 * and recheck polar bit and decode_status
 * decode_status go back 1 buf,
 * NEW -> ONE/ZERO; ONE/ZERO -> NEW
 *
 * jump out of buf limit, return < 0
 */
int w83667hg_jump_frame(struct irctl *ir, u8 frame_num, unsigned int *buf_num, u8 *out_bit, u8 *decode_status);

/* jump MCE head sync signal (1111 11)
 *
 * after jump signal head, current pulse number = 7
 * record current buf number of irctl buf
 * record current pulse's bit and decode_status
 *
 * jump out of buf limit, return < 0
 */
int w83667hg_jump_head(struct irctl *ir, unsigned int *buf_num);

#define KEY_IDENTIFICATION        0x04
#define MOUSE_IDENTIFICATION      0x01
#define IDENTIFICATION_CHECK_BIT  0xF

/* check mce identification
 *
 * compare identification and cur_iden
 * same return 0
 * else return < 0, need continue check
 */
int w83667hg_check_identification(u8 val, u8 identification, u8 *cur_iden);

/* jump mce identification
 * can use on key, mouse and controller signals
 *
 * jump and search identification parts
 * record current buf number of irctl buf
 *
 * jump all pulse until find identification
 *
 *
 *
 * problem 1:
 * When jump_identification() stops on a double_width_len pulse, the buf_num will be the
 * second pulse of the doule_width_len frame.
 *
 * one_width_len pulse:
 *   _    _
 * .| |_| |_.      (1010)
 *     ^
 *     |
 *   stop: jumped frame_num
 *
 *
 * problem 1: double_width_len pulse:
 *   __    _
 * .|  |__| |.      (11001)
 *       ^
 *       |
 *      stop: jumped frame_num + 1, buf_num + 2
 *
 * solution:
 * Now buf_num is the next double-pulse-width buf.
 * Just decrease buf_num by one can fix this problem.
 *
 *
 * After jump iden, output may diff by decode status:
 * 1)
 * For status ONE and ZERO, all decode work is done.
 * Set status as NEW and inverse out_bit
 * in fact, bit is not important, for a buf can indicate one frame, whether its
 * length is one or two, the bit can be ignored
 *
 * after jump frame, all status is NEW! reset them as inverse polar bit
 *
 *
 *
 *
 * can not find identification, return < 0
 */
int w83667hg_jump_identification(struct irctl *ir, u8 identification, unsigned int *buf_num, u8 *out_bit, u8 *decode_status);


/* the frame number between identification and code */
#define KEY_KEYCODE_FRAME_AFTER_I_START   16  /*//32*/
#define KEY_FRAME_AFTER_JUMP              16  /*//64*/
#define KEY_SUBMIT_KEYCODE_FRAME_NUM      8


#define MOUSE_DELTAY_PULSE_AFTER_I_START  8
#define MOUSE_CHECKSUM_FRAME_AFTER_JUMP   16
#define MOUSE_SUBMIT_DELTAY_FRAME_NUM     7
#define MOUSE_SUBMIT_DELTAX_FRAME_NUM     14
#define MOUSE_SUBMIT_R_FRAME_NUM          15
#define MOUSE_SUBMIT_L_FRAME_NUM          16


/* FIXME, not sure this value, select a large number*/
#define PULSE_SILENT_LEN_LOW  90


/* set keycode and shiftmask for keyboard */
void w83667hg_set_key(u8 *set_frame, u8 val, u8 *keycode, u8 *shiftmask);

/* decode mce keyboard signal and send data to input
 *
 * 1) jump mce keyboard signal sync head (1111 11)
 * 2) jump identification (0100)
 * 	pulse number before identification is not sure
 * 3) jump "unknown", "C" and "CodeSet" parts in mce keyboard signal
 * 4) deocde "KeyCode" and "ShiftMask" parts in mce key signal
 * 5) send keycode to input by array usb_kbd_keycode
 * 6) send shiftmask to input by array usb_kbd_keycode
 */
void w83667hg_submit_key(struct irctl *ir);

/* set deltax, deltay, x, y for mce mouse */
void	w83667hg_set_mouse(u8 *set_frame, u8 val, u8 *deltay, u8 *deltax, u8 *clickrl);

/* decode mce mouse signal and send data to input
 *
 * 1) jump mce mouse signal sync head (1111 11)
 * 2) jump identification (0001)
 * 	pulse number before identification is not sure
 * 3) jump "unknown" part in mce mouse signal
 * 4) deocde "deltay", "deltax", "r" and "l" parts in mce mouse signal
 * 5) send these parts to input
 */
void w83667hg_submit_mouse(struct irctl *ir);



#define BIT_PULSE_MASK 0x80
#define BIT_LEN  0x7f



/* "10"=>1 (+2), "01"=>0 (+2) */
#define ST_DECODE_NEW  0x01
#define ST_DECODE_ZERO 0x02
#define ST_DECODE_ONE  0x04

/* RC6 sync head: 1111 11
 * 6 * 444us / 50us (sample period) * 0.85 (inaccuracy)
 */
#define HEAD_SYNC_LEN_LOW  45
/* 7 pulses long, for inaccuracy */
#define HEAD_SYNC_LEN_HIGH 71

#define ONE_PULSE_LEN_LOW   2
#define ONE_PULSE_LEN_HIGH  10
#define TWO_PULSE_LEN_LOW   10
#define TWO_PULSE_LEN_HIGH  15


#endif
