# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $ 

EAPI=2

inherit eutils linux-info

DESCRIPTION="A communications program for HP-48 and 49 series calculators."
HOMEPAGE="http://hptalx.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=app-misc/ckermit-8.0
		>=dev-libs/glib-2.4
		>=dev-libs/libxml2-2.5
		>=x11-libs/gtk+-2.4"
DEPEND="${RDEPEND}
		app-text/docbook-sgml-dtd:4.5"

CONFIG_CHECK="~USB_SERIAL_HP4X"
ERROR_USB_SERIAL_HP4X="
  You must enable the kernel's HP4X USB driver to use hptalx with HP-49g+ or later calculators.

  (Device Drivers -> USB support -> USB Serial Converter support -> USB HP4x Calculators support)
"

src_prepare() {
	epatch "${FILESDIR}/${P}-preamble.patch"
	epatch "${FILESDIR}/${P}-hptalx.patch"
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS BUGS ChangeLog NEWS NOTE README README.hp49gplus ToDo
	mv "${D}"usr/share/doc/${PN}/* "${D}"usr/share/doc/${PF}/
	rm -r "${D}"usr/share/doc/${PN}
}

pkg_postinst() {
	echo
	elog "To put the calculator in Server Mode:"
	elog "  HP 48: Press <Right Shift>, then <Right Arrow> on the calculator."
	elog "  HP 49/50: Press and hold <Right Shift> and press <Right Arrow>"
	elog "  on the calculator."
	elog
	elog "To use HPTalx with HP-49g+/50g Calculators, see"
	elog "/usr/share/doc/${PF}/README.hp49gplus"
	echo
}
