# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"

inherit distutils

DESCRIPTION="An easy to use multimedia transcoder for the GNOME Desktop"
HOMEPAGE="http://programmer-art.org/projects/arista-transcoder"
SRC_URI="http://programmer-art.org/media/releases/arista-transcoder/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="+faac +xvid libnotify nautilus x264"

DEPEND="dev-python/setuptools"
RDEPEND="dev-lang/python[xml]
	>=x11-libs/gtk+-2.16
	>=dev-python/pygtk-2.16
	dev-python/pygobject
	dev-python/pycairo
	dev-python/gconf-python
	dev-python/dbus-python
	>=media-libs/gstreamer-0.10.22
	dev-python/gst-python
	media-libs/gst-plugins-base:0.10
	media-libs/gst-plugins-bad
	media-libs/gst-plugins-good:0.10
	media-libs/gst-plugins-ugly:0.10
	media-plugins/gst-plugins-meta:0.10
	media-plugins/gst-plugins-ffmpeg:0.10
	libnotify? ( dev-python/notify-python )
	nautilus? ( dev-python/nautilus-python )
	faac? ( media-plugins/gst-plugins-faac:0.10 )
	xvid? ( media-plugins/gst-plugins-xvid:0.10 )
	x264? ( media-plugins/gst-plugins-x264:0.10 )"
RESTRICT_PYTHON_ABIS="3.*"

DOCS="README.releases LICENSE AUTHORS"

pkg_postinst() {
	einfo "If you find that a format you want is not supported in Arista,"
	einfo "please make sure that you have the corresponding USE-flag enabled"
	einfo "in media-plugins/gst-plugins-meta"
}
