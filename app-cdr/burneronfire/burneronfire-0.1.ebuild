# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"

inherit distutils eutils

MY_PN="BurnerOnFire"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="BurnerOnFire is multi-threaded tool that can write content to multiple CD/DVD burners simultaneously"
HOMEPAGE="http://www.kiberpipa.org/burneronfire/"
SRC_URI="http://pypi.python.org/packages/source/${MY_PN:0:1}/${MY_PN}/${MY_P}.tar.gz"

LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE="doc gnome"

DEPEND="dev-python/setuptools
	dev-python/pygobject
	dev-python/minihallib
	app-cdr/cdrkit
	gtk? ( dev-python/pygtk )
	doc? ( >=dev-python/sphinx-0.6 )"
RDEPEND="${DEPEND}"
RESTRICT_PYTHON_ABIS="3.*"

S="${WORKDIR}/${MY_P}"

src_compile(){
	distutils_src_compile

	if use doc; then
		cd "${S}/docs"
		PYTHONPATH=.. emake html || die "Building of documentation failed"
	fi
}

src_install(){
	distutils_src_install

	if use doc; then
		dohtml -r docs/build/html/* || die "Installation of documentation failed"
	fi

	if use gnome; then
		ebegin "Installing menu entry"
		newicon "${S}/${PN}/icons/burnthem.png" ${PN}.png
		make_desktop_entry ${PN} ${MY_PN} ${PN} "System;Utility;DiscBurning"
		eend $?
	fi
}

pkg_postinst() {
	elog "Be sure to read notes/warnings about usage at"
	elog "http://www.kiberpipa.org/burneronfire/#notes"
}
